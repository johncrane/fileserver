// This file server serves the contents of the specified directory and starts the server on port 8080.
// Usage: go run main.go <directory>
package main

import (
	"log"
	"net/http"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Usage: go run main.go <directory>")
	}
	dirPath := os.Args[1]

	// dirPath := "/home/johncrane/code/jayhat-portfolio/public"

	// Create a file server that serves the contents of the specified directory.
	fileServer := http.FileServer(http.Dir(dirPath))

	// Register the file server as the handler for all requests.
	http.Handle("/", fileServer)

	// Start the server on port 8080.
	log.Println("Starting server on :8080")
	log.Println("Serving directory: ", dirPath)
	log.Println("Ctrl+C to stop the server.")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
